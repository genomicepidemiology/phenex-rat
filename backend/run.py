"""
Run this file with uwsgi
$ cd /srv/www/htdocs/config/services
$ uwsgi --ini app.uwsgi.development.ini --http-socket /var/run/app.uwsgi.sock
"""
from app import app
from app import db_sqlite
from config.constants import APP_HOST
from config.constants import APP_DEBUG
from config.constants import APP_TESTING
from config.constants import APP_THREADED


# link: https://stackoverflow.com/questions/34615743/unable-to-load-configuration-from-uwsgi#answer-37175998
if __name__ == "__main__":
    db_sqlite.create_all()
    app.run(debug=APP_DEBUG, testing=APP_TESTING, host=APP_HOST, threaded=APP_THREADED)
