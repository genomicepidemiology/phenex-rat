import os
import json

HOST = os.getenv('HOST', 'phenex.local')
CODE_BASE = os.getenv('CODE_BASE', 'development')


APP_DEBUG = os.getenv('DEBUG', False) if (type(os.getenv('DEBUG', False)) == bool) else json.loads(os.getenv('DEBUG'))
APP_TESTING = os.getenv('TESTING', False) if (type(os.getenv('TESTING', False)) == bool) else json.loads(os.getenv('TESTING'))

APP_LOG = os.getenv('PATH_LOG_APPLICATION', '/srv/www/htdocs/storage/log/app.log')
APP_HOST = '0.0.0.0'
APP_THREADED = True
APP_PROTOCOL = 'http' if CODE_BASE == 'development' else 'https'

APP_DEBUG = os.getenv('DEBUG', False) if (type(os.getenv('DEBUG', False)) == bool) else json.loads(os.getenv('DEBUG'))
APP_TESTING = os.getenv('TESTING', False) if (type(os.getenv('TESTING', False)) == bool) else json.loads(os.getenv('TESTING'))

DB_MONGO_PORT = int(os.getenv('DB_MONGO_PORT', 27017))
DB_MONGO_HOST = os.getenv('DB_MONGO_HOST', 'database-mongo')
DB_MONGO_NAME = os.getenv('DB_MONGO_NAME', 'phenex')
DB_MONGO_NAME = DB_MONGO_NAME if not APP_TESTING else DB_MONGO_NAME + '_test'

DB_SQLITE_PATH = os.getenv('DB_SQLITE_PATH', '/srv/www/htdocs/storage/permastore/sqlite/database.sqlite3')
DB_SQLITE_PATH = DB_SQLITE_PATH if not APP_TESTING else DB_SQLITE_PATH.replace('database.sqlite3', 'database_test.sqlite3')
