# SQLite3

```
from app import db_sqlite

db_sqlite.execute('CREATE TABLE posts (id INT PRIMARY KEY, content TEXT, active INTEGER)')
```


# SQL Alchemy
```
from app import db_sqlite

db_sqlite.create_all()
```
