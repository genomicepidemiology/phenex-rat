// this packages were installed via npm
var del = require('del');
var argv = require('yargs').argv;
var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpIf = require('gulp-if');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var gpConcat = require('gulp-concat');
var gulpCssnano = require('gulp-cssnano');
var browserSync = require('browser-sync');

var ASSETS_PATH = '../assets/*';
var ASSETS_PATH_1ST_LEVEL = '../assets/**/*';
var ASSETS_PATH_2ND_LEVEL = '../assets/**/**/*';
var ASSETS_IMG_PATH = '../static/../assets/img';
var SCSS_PATH = '../scss/*.scss';
var SCSS_PATH_1ST_LEVEL = '../scss/**/*.scss';
var SCSS_PATH_2ND_LEVEL = '../scss/**/**/*.scss';
var STATIC_ROOT_PATH = '../static';
var STATIC_ASSETS_PATH = '../static/assets';
var STATIC_ASSETS_FONTS_PATH = '../static/css/fonts';
var STATIC_JS_ROOT_PATH = '../static/js';
var STATIC_JS_LIBS_PATH = '../static/js/libs';
var STATIC_CSS_ROOT_PATH = '../static/css';
var STATIC_CSS_LIBS_PATH = '../static/css/libs';
var STATIC_HTML_PATH = '../static/html';
var STATIC_INDEX = '../static/index.html';

// Remove comiled files
gulp.task('clean', function() {
  return del('../static/*');
});


// Script Tasks
gulp.task('load_resources', function() {

  var RESOURCES = {
    'js': {
      'production': [
        '../node_modules/vue/dist/vue.min.js',
        '../node_modules/vue/dist/bootstrap.min.js',
        '../node_modules/vue-resource/dist/vue-resource.min.js',
        '../node_modules/angular/angular.min.js',
        '../node_modules/jquery/dist/jquery.min.js',
      ],
      'development': [
        '../node_modules/vue/dist/vue.js',
        '../node_modules/vue-resource/dist/vue-resource.js',
        '../node_modules/angular/angular.js',
        '../node_modules/jquery/dist/jquery.js',
        '../node_modules/bootstrap/dist/js/bootstrap.js',
      ]
    },
    'css': {
      'production': [
        '../node_modules/bootstrap/dist/css/bootstrap.min.css',
      ],
      'development': [
        '../node_modules/bootstrap/dist/css/bootstrap.css',
        '../node_modules/font-awesome/css/font-awesome.css',
      ]
    },
    'maps': {
      'css': [
        '../node_modules/bootstrap/dist/css/bootstrap.css.map',
        '../node_modules/font-awesome/css/font-awesome.css.map',
      ],
      'js': [
        '../node_modules/bootstrap/dist/js/bootstrap.js.map',
      ]
    },
    'app': {
      'js': [
        '../resources/app/app.js',
      ]
    }
  }

  gulp.src(RESOURCES['app']['js'])
    .pipe(gpConcat('concat.js'))
    .pipe(gulpIf(argv.production, uglify({mangle: false})))
    .pipe(gulpIf(argv.production, rename('backend.app.min.js'), rename('backend.app.js')))
    .pipe(gulp.dest(STATIC_JS_ROOT_PATH));

  var libs_js = (argv.production) ? RESOURCES['js']['production'] : RESOURCES['js']['development'];
  gulp.src(libs_js)
    .pipe(gulpIf(argv.production || argv.testing, gpConcat('vue.concat.js')))
    .pipe(gulpIf(argv.production, uglify()))
    .pipe(gulpIf(argv.production, rename('vendors.min.js')))
    .pipe(gulpIf(argv.testing, rename('vendors.js')))
    .pipe(gulp.dest(STATIC_JS_LIBS_PATH));

  var libs_css = (argv.production) ? RESOURCES['css']['production'] : RESOURCES['css']['development'];
  gulp.src(libs_css)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulpIf(argv.production || argv.testing, gpConcat('concat.css')))
    .pipe(gulpIf(argv.production, gulpCssnano()))
    .pipe(gulpIf(argv.production, rename('vendors.min.css')))
    .pipe(gulpIf(argv.testing, rename('vendors.css')))
    .pipe(gulp.dest(STATIC_CSS_LIBS_PATH));

  var libs_maps_js = RESOURCES['maps']['js'];
  var libs_maps_css = RESOURCES['maps']['css'];
  gulp.src(libs_maps_js).pipe(gulp.dest(STATIC_JS_LIBS_PATH));
  gulp.src(libs_maps_css).pipe(gulp.dest(STATIC_CSS_LIBS_PATH));

  gulp.src([SCSS_PATH, SCSS_PATH_1ST_LEVEL, SCSS_PATH_2ND_LEVEL])
    .pipe(sass().on('error', sass.logError))
    .pipe(gulpIf(argv.production, gulpCssnano()))
    .pipe(gulpIf(argv.production, rename('styles.min.css'), rename('styles.css')))
    .pipe(gulp.dest(STATIC_CSS_ROOT_PATH));
});

// Browser sync
gulp.task('browser-sync', ['load_resources'], browserSync.reload);

// Watch Tasks
gulp.task('watch', function() {
  browserSync({
      proxy: 'http://' + process.env.HOST + '/',
      port: 3001,
      open: false,
  });
  gulp.watch([
      SCSS_PATH, SCSS_PATH_1ST_LEVEL, SCSS_PATH_2ND_LEVEL,
      ASSETS_PATH, ASSETS_PATH_1ST_LEVEL, ASSETS_PATH_2ND_LEVEL
  ], ['browser-sync', 'load_resources']);
});

// Default Tasks 'gulp'
// gulp.task('default', ['clean'], function() {
gulp.task('default', [], function() {
  gulp.start('load_resources')
});
