#!/bin/bash

# Set timezone
rm /etc/localtime
echo $TZ > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

## UPDATE PIP
pip3 install --upgrade pip

## INSTALL PYTHON PACKAGES
pip3 install -r requirements.txt

## ## ## NodeJS TASKS
npm install
yarn install

## REMOVE DEFAULT CONFIGURATION FOR NGINX & APACHE WEB_SERVER SERVER
rm /etc/nginx/sites-enabled/default
rm /etc/nginx/sites-available/default
rm /etc/apache2/sites-enabled/000-default.conf
rm /etc/apache2/sites-available/000-default.conf

## NGINX CONFIG
cp config/services/nginx.conf /etc/nginx/nginx.conf

## SUPERVISOR CONFIG
ln -s /srv/www/htdocs/config/services/app.supervisor.${ENVIRONMENT}.conf /etc/supervisor/conf.d/app.conf

if [ "$CODE_BASE" = "development" ]; then
 gulp --gulpfile scripts/gulpfile.js
 gulp --gulpfile scripts/gulpfile.js
fi

if [ "$CODE_BASE" = "testing" ]; then
 gulp --gulpfile scripts/gulpfile.js --testing
 gulp --gulpfile scripts/gulpfile.js --testing
fi

if [ "$CODE_BASE" = "production" ]; then
 gulp --gulpfile scripts/gulpfile.js --production
 gulp --gulpfile scripts/gulpfile.js --production
fi

## START SERVICES
/usr/sbin/service rsyslog start
#/usr/sbin/service cron start
/usr/bin/supervisord


if [ "$APPLICATION_DEPLOYMENT" = "uwsgi" ]; then
    /usr/bin/supervisorctl start app_uwsgi
    sleep infinity
fi

if [ "$APPLICATION_DEPLOYMENT" = "uwsgi_nginx" ]; then
  /usr/bin/supervisorctl start app_uwsgi_nginx

  ln -s /srv/www/htdocs/config/services/app.nginx.uwsgi.${ENVIRONMENT}.conf /etc/nginx/sites-available/app.conf
  ln -s /etc/nginx/sites-available/app.conf /etc/nginx/sites-enabled/app.conf

  /usr/sbin/nginx -g "daemon off;"
fi

if [ "$APPLICATION_DEPLOYMENT" = "gunicorn" ]; then
  /usr/bin/supervisorctl start app_gunicorn
  sleep infinity
fi

if [ "$APPLICATION_DEPLOYMENT" = "gunicorn_nginx" ]; then
  /usr/bin/supervisorctl start app_gunicorn_nginx

  ln -s /srv/www/htdocs/config/services/app.nginx.gunicorn.${ENVIRONMENT}.conf /etc/nginx/sites-available/app.conf
  ln -s /etc/nginx/sites-available/app.conf /etc/nginx/sites-enabled/app.conf

  /usr/sbin/nginx -g "daemon off;"
fi

#if [ "$WEB_SERVER" = "apache" ]; then
#  /usr/sbin/apache2ctl -D FOREGROUND
#fi
