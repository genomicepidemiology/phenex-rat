from app import db_mongo
from app import db_sqlite

Model = db_sqlite.Model
Column = db_sqlite.Column
String = db_sqlite.String
Integer = db_sqlite.Integer

IntField = db_mongo.IntField
StringField = db_mongo.StringField
BooleanField = db_mongo.BooleanField
DynamicDocument = db_mongo.DynamicDocument


class Post(DynamicDocument):
    index = IntField()
    content = StringField()
    active = BooleanField(default=False)


class Student(Model):
    __tablename__ = 'students'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    department = Column(String(50))

    # def __init__(self, name, department):
    #     self.name = name
    #     self.department = department
