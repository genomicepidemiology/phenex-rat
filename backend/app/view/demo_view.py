
from flask_classy import route
from flask_classy import FlaskView


class DemoView(FlaskView):
    view = 'DemoView'
    route_base = '/admin'

    @route('/', methods=['GET'])
    def index(self):
        return 'Hello, Backend!'
