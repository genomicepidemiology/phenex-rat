from flask import jsonify
from flask_classy import route
from flask_classy import FlaskView


class DemoView(FlaskView):
    view = 'DemoView'
    route_base = '/api/v1/core'

    @route('/', methods=['GET'])
    def index(self):
        data = [
            {
                'id': 1,
                'title': u'Domin Driven Design',
                'read': True
            },
            {
                'id': 2,
                'title': u'DevOps Handbook',
                'read': False
            }
        ]
        return jsonify({'books': data})
