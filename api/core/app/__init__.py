from flask import Flask
from flask_mongoengine import MongoEngine
from config.constants import DB_MONGO_HOST
from config.constants import DB_MONGO_PORT
from config.constants import DB_MONGO_NAME

app = Flask(__name__)

# Disable trailing slashes for all requests
app.url_map.strict_slashes = False

app.config['MONGODB_DB'] = DB_MONGO_NAME
app.config['MONGODB_HOST'] = DB_MONGO_HOST
app.config['MONGODB_PORT'] = DB_MONGO_PORT

db_mongo = MongoEngine(app)

from app.view.demo_view import DemoView

DemoView.register(app)
# DemoView.register(app, route_prefix='/')
