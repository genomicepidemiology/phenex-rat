import multiprocessing

bind = 'unix:/var/run/app.gunicorn.sock'
pythonpath = '/srv/www/htdocs/run'
user = 'root'
group = 'www-data'
workers = multiprocessing.cpu_count() * 2 + 1
umask = 105
loglevel = 'debug'
accesslog = '/srv/www/htdocs/storage/log/access.log'
errorlog = '/srv/www/htdocs/storage/log/error.log'
reload = True
enable_stdio_inheritance = True
