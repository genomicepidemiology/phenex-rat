#!/bin/bash

# Set timezone
rm /etc/localtime
echo $TZ > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

## NodeJS TASKS
npm install
yarn install

## REMOVE DEFAULT CONFIGURATION FOR NGINX & APACHE WEB_SERVER SERVER
rm /etc/nginx/sites-enabled/default
rm /etc/nginx/sites-available/default
rm /etc/apache2/sites-enabled/000-default.conf
rm /etc/apache2/sites-available/000-default.conf

## NGINX CONFIG
cp config/services/nginx.conf /etc/nginx/nginx.conf
ln -s /srv/www/htdocs/config/services/app.nginx.${ENVIRONMENT}.conf /etc/nginx/sites-available/app.conf
ln -s /etc/nginx/sites-available/app.conf /etc/nginx/sites-enabled/app.conf

 if [ "$CODE_BASE" = "development" ]; then
   gulp --gulpfile scripts/gulpfile.js
 fi

 if [ "$CODE_BASE" = "testing" ]; then
   gulp --gulpfile scripts/gulpfile.js --testing
 fi

 if [ "$CODE_BASE" = "production" ]; then
   gulp --gulpfile scripts/gulpfile.js --production
 fi

## START SERVICES
/usr/sbin/service rsyslog start

## START WEB_SERVER
if [ "$WEB_SERVER" = "nginx" ]; then
  /usr/sbin/nginx -g "daemon off;"
else
  /usr/sbin/apache2ctl -D FOREGROUND
fi
