var app = angular.module("app", []);

app.controller("HelloController", function($scope, $http) {

  $http({
      method: 'GET',
      url: 'https://reqres.in/api/users'
    }).then(function(response) {
      console.log('success', response.data)
      $scope.angular_data = response.data;
    },
    function(response) {
      console.log('error', response.data)
    });

});
