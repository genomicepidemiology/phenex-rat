var app = new Vue({
  el: '#app',
  data: {
    vue_data: 'Hello Vue!'
  },
  methods: {
    loadData: function() {
      this.$http.get('/api/v1/core').then(response => {
        this.vue_data = response.body;
      });
    }
  }
});
